# 智慧党建

#### 介绍
开腾网络经过对智慧党建市场的长期调研，综合当下各大机构对于党建产品的功能需求，着重于用户体验，自主研发了一套集学习、娱乐、互动、办公等功能为一体的万岳智慧党建系统，满足用户对于组织学习、考试答题、党建参阅、党员管理、缴纳党委、档案调动等多种活动的场景需求。

#### 软件架构
![输入图片说明](%E6%99%BA%E6%85%A7%E5%85%9A%E5%BB%BA1.png)

#### 设计图
![输入图片说明](%E9%A6%96%E9%A1%B5.png)
![输入图片说明](%E7%AD%94%E9%A2%98%E8%80%83%E8%AF%95.png)
![输入图片说明](%E5%85%9A%E5%BB%BA%E8%A7%86%E9%A2%91.png)
![输入图片说明](%E5%85%9A%E5%BB%BA%E6%8E%92%E5%90%8D.png)
![输入图片说明](%E9%9A%8F%E6%89%8B%E6%8B%8D-%E5%8F%91%E5%B8%83.png)
![输入图片说明](%E5%85%9A%E5%BB%BA%E6%9C%89%E5%A3%B0-%E6%92%AD%E6%94%BE.png)

### :tw-1f427: QQ客服
 QQ号:2383177967 
 :fa-fax: 联系电话：15574854597
###  客服二维码
![输入图片说明](https://images.gitee.com/uploads/images/2020/0721/184544_98f5627e_4940443.jpeg "kf.jpg")


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
